#include <stdio.h>


#define CMRATIO 5000
#define CIRCLER 500
#define STROKEW 10
#define BUFFER 1000


typedef struct
{
    int minx,maxx;
    int miny,maxy;
}rectangle_t;

typedef struct 
{
    int x,y;
    int count;
}kuju_t;

rectangle_t findViewBox(FILE *f, const char *format){
	rectangle_t r;
	int first = 1;
	int x, y;
	while(!feof(f)){
		if(fscanf(f, format, &x, &y) == 2){
			if(first){
				r.minx = r.maxx = x; // algväärtustamine
				r.miny = r.maxy = y;
				first = 0;
			}
			if(x < r.minx)
				r.minx = x;
			if(x > r.maxx)
				r.maxx = x;
			if(y < r.miny)
				r.miny = y;
			if(y > r.maxy)
				r.maxy = y;
		}
	}
	rewind(f); // faili algusess tagasi
	r.minx = r.minx - BUFFER; // lisame puhvri
	r.maxx = r.maxx + BUFFER;
	r.miny = r.miny - BUFFER;
	r.maxy = r.maxy + BUFFER;
	return r;
}


int main(void)
{
    FILE *in, *out;
    in = fopen("puuraugud.txt","r");
    out = fopen("puur.svg","w");
    if(out == NULL || in == NULL)
    {
        printf("Ei saanud faili avada\n");
        return  1;
    }
    rectangle_t viewBox = findViewBox(in,"%*s %*d %d %d");
    
    int w,h;
    w = (viewBox.maxx - viewBox.minx)/CMRATIO;
    h = (viewBox.maxy - viewBox.miny)/CMRATIO;
    
    int veerg, rida;
    printf("Sisesta ruudustiku suurus(veergude arv ja ridad arv): ");
    if(scanf("%d %d",&veerg, &rida) < 2)
    {
        printf("Vigane sisestus\n");
    }
    
    fprintf(out, "<?xml version=\"1.0\"?>\n");
	fprintf(out, "<svg width=\"%dcm\" height=\"%dcm\" viewBox=\"%d %d %d %d\"\n", w, h, 
		viewBox.minx, 0, viewBox.maxx - viewBox.minx, viewBox.maxy - viewBox.miny);
	fprintf(out, "\txmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\n");
    
    
    kuju_t grid[rida][veerg];
    
    //mõõtmed
    int kujuW   = (viewBox.maxx - viewBox.minx)/veerg;
    int kujuH = (viewBox.maxy - viewBox.miny)/rida;
    
    //Positsioneerimine
    for(int i = 0 ; i < rida; i ++)
    {
        for(int j = 0 ; j  <veerg;j++)
        {
            grid[i][j].x = j * kujuW + viewBox.minx;
            grid[i][j].y = i * kujuH + viewBox.miny;
            grid[i][j].count = 0; 
        }
    }

    int well_x ,well_y,maxCount = 0;
    while(!feof(in))
    {
        if(fscanf(in,"%*s %*d %d %d",&well_x,&well_y) == 2)
        {
            for(int i = 0 ; i < rida; i ++)
            {
                for(int j = 0; j < veerg;j++)
                {
                    //Kontrollime kas kaevude kordinaadid on kuju parameetrites
                    if(well_x >= grid[i][j].x && well_x< grid[i][j] .x + kujuW &&
                        well_y >= grid[i][j].y && well_y < grid[i][j ].y + kujuH)
                        {
                            grid[i][j].count ++;

                            if(grid[i][j].count> maxCount)
                            {
                                maxCount = grid[i][j].count;
                            }
                            //printf("%d\n",grid[i][j].count);
                        }
                       
                }

            }
  
        }
    }
    
    for(int i = 0 ; i < rida; i ++)
    {
        for(int j = 0 ; j < veerg;j++)
        {
            int r, g, b;
            if(grid[i][j].count == 0)
            {
                r = 255;
                g = 255;
                b = 255;
            }
            else if(grid[i][j].count  == maxCount)
            {
                r = 136;
                g = 8;
                b = 8;
            }
            else if(grid[i][j].count < maxCount * 0.1)
            {
                r = 149;
                g = 255;
                b= 102;
            }
            else if(grid[i][j].count<maxCount * 0.17)
            {
                r = 0;
                g= 255;
                b = 0;
            }
            else if(grid[i][j].count < maxCount * 0.4)
            {
                r = 34;
                g = 139;
                b = 34;
            }
            else if(grid[i][j].count < maxCount * 0.61)
            {
                r = 238;
                g = 75;
                b = 43;
            }
            else if(grid[i][j].count < maxCount * 0.79)
            {
                r = 210;
                g = 4;
                b = 45;
            }
         fprintf(out, "<rect x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\" fill=\"rgb(%d, %d, %d)\" stroke=\"black\" stroke-width=\"%d\"/>\n",
                    grid[i][j].x, viewBox.maxy - grid[i][j].y - kujuH, kujuW, kujuH, r, g, b, STROKEW);
       
        }
    }
    
    
    
    fprintf(out,"</svg>");
    fclose(in);
    fclose(out);
    
   printf("SVG fail genereeritud!\n");
    
    
    
    return 0;
}
